const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const courseRoutes =  require("./routes/courseRoutes");

const app = express();

const port = 4000;

// connecting to our mongoDB

mongoose.connect("mongodb+srv://joram_182:joramape182@zuitt-bootcamp.kq3szvv.mongodb.net/s37-s41?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

mongoose.connection.once('open', () => console.log('Now connected to mongoDB Atlas!'));

// cors stand for Cross-origin Resource sgaring.
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Declaring main routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`API is now online on port ${port}`));